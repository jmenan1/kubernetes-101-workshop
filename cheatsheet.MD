## Part 2 - Kubernetes

- [Part 2 - Kubernetes](#Part-2---Kubernetes)
  - [Start etcd](#Start-etcd)
  - [Start the API server](#Start-the-API-server)
  - [Verify the api server](#Verify-the-api-server)
  - [Maybe we can create a deployment ?](#Maybe-we-can-create-a-deployment-)
  - [Verify pods](#Verify-pods)
  - [Wait ? Why is there no pods ? Let's start the controller manager](#Wait--Why-is-there-no-pods--Lets-start-the-controller-manager)
  - [Verify the events](#Verify-the-events)
  - [There is an error with a service account. Let's add an option](#There-is-an-error-with-a-service-account-Lets-add-an-option)
  - [Is this better ?](#Is-this-better-)
  - [No... There is no scheduler and no nodes ! Let's start dockerd](#No-There-is-no-scheduler-and-no-nodes--Lets-start-dockerd)
  - [Verify it works](#Verify-it-works)
  - [Now, let's add a node. But wait, it's a bit complicated. Let's start with generate a kubeconfig](#Now-lets-add-a-node-But-wait-its-a-bit-complicated-Lets-start-with-generate-a-kubeconfig)
  - [And now we can start kubelet](#And-now-we-can-start-kubelet)
  - [Do we have a node ?](#Do-we-have-a-node-)
  - [And our pod ?](#And-our-pod-)
  - [Crap... Oh wait... We don't have a scheduler yet](#Crap-Oh-wait-We-dont-have-a-scheduler-yet)
  - [Now our pod should run](#Now-our-pod-should-run)
  - [Let's connect to it](#Lets-connect-to-it)
  - [Now let's expose it via a service](#Now-lets-expose-it-via-a-service)
  - [Timeout ? Oh wait, the proxy !!](#Timeout--Oh-wait-the-proxy-)
  - [Let's curl it](#Lets-curl-it)
  - [Let's see what it did with iptables](#Lets-see-what-it-did-with-iptables)
  - [SEP stands for ServiceEndPoint](#SEP-stands-for-ServiceEndPoint)
  - [Scale the service now](#Scale-the-service-now)
  - [And see what iptables did](#And-see-what-iptables-did)
  - [Create Helm chart](#Create-Helm-chart)

### Start etcd
etcd
### Start the API server
sudo kube-apiserver --etcd-servers http://localhost:2379
### Verify the api server
kubectl get all
kubectl get nodes
kubectl get componentstatuses
### Maybe we can create a deployment ?
kubectl create deployment web --image=nginx
### Verify pods
kubectl get all
### Wait ? Why is there no pods ? Let's start the controller manager
kube-controller-manager --master http://localhost:8080
### Verify the events
kubectl get events
### There is an error with a service account. Let's add an option
sudo kube-apiserver --etcd-servers http://localhost:2379 --disable-admission-plugins=ServiceAccount
### Is this better ?
kubectl get all
### No... There is no scheduler and no nodes ! Let's start dockerd
./scripts/31-download-prerequisites-worker.sh
sudo dockerd
### Verify it works
sudo docker run alpine echo HelloSunnyTech
### Now, let's add a node. But wait, it's a bit complicated. Let's start with generate a kubeconfig
kubectl --kubeconfig kubeconfig.kubelet config set-cluster localhost --server http://localhost:8080
kubectl --kubeconfig kubeconfig.kubelet config set-context localhost --cluster localhost
kubectl --kubeconfig kubeconfig.kubelet config use-context localhost
### And now we can start kubelet
sudo kubelet --kubeconfig kubeconfig.kubelet
### Do we have a node ?
kubectl get nodes
### And our pod ?
kubectl get pods
### Crap... Oh wait... We don't have a scheduler yet
kube-scheduler --master http://localhost:8080
### Now our pod should run
kubectl get all
### Let's connect to it
kubectl get pods -o wide
curl $IP
### Now let's expose it via a service
kubectl expose deployment web --port=80
kubectl get svc web
curl $IP
### Timeout ? Oh wait, the proxy !!
sudo kube-proxy --master http://localhost:8080
### Let's curl it
curl $IP
### Let's see what it did with iptables
sudo iptables -t nat -L OUTPUT
sudo iptables -t nat -L KUBE-SERVICES
sudo iptables -t nat -L KUBE-SVC-XXX
### SEP stands for ServiceEndPoint
sudo iptables -t nat -L KUBE-SEP-YYY
### Scale the service now
kubectl scale deployment web --replicas=4
### And see what iptables did
sudo iptables -t nat -L KUBE-SVC-XXX
### Create Helm chart
kompose convert --chart -f docker-compose.images.yml